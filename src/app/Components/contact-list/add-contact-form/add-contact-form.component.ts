import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {FormGroup, FormControl, Validators} from '@angular/forms'
import {Contact} from '../../../models/contact.model'


@Component({
  selector: 'app-add-contact-form',
  templateUrl: './add-contact-form.component.html',
  styleUrls: ['./add-contact-form.component.scss']
})
export class AddContactFormComponent implements OnInit{

  @Output() public addContact: EventEmitter<Contact> = new EventEmitter<Contact>();

  public isShowForm:boolean = false;

  public addContactForm: FormGroup;

  public showForm():void {
    this.isShowForm = !this.isShowForm;
  }

  ngOnInit(): void {
    this.addContactForm = new FormGroup({
      //add name of fields(in form) and set base value
      name: new FormControl(null,{validators:[Validators.required]}),
      phone: new FormControl(null,{validators:[Validators.required]})
    })
  }

  public onSubmit():void {
    let name = this.addContactForm.value.name;
    let phone = this.addContactForm.value.phone;
    let contact = new Contact(name, phone);
    this.addContact.emit(contact);
    this.addContactForm.reset();
    this.isShowForm = false;
  }
}
